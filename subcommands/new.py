import os
import shutil
import pathlib

def new(path, args):
    APP_DIR = args.target_dir

    try:
        os.mkdir(APP_DIR)
    except FileExistsError:
        print("ERROR: File already exists!")
        exit()

    os.mkdir(APP_DIR + '/src')
    shutil.copyfile(path + '/' + 'templates/main.c.template', APP_DIR +"/src/main.c")

    shutil.copyfile(path + '/' + 'templates/app.template', APP_DIR +"/CMakeLists.txt")

    path = pathlib.Path(APP_DIR, 'prj.conf')
    path.touch()
