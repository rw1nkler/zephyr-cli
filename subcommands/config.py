import os
import subprocess

def config(path, args):

    ZEPHYR_BASE = os.getenv('ZEPHYR_BASE')
    zephyr_env_path = ZEPHYR_BASE + '/zephyr-env.sh'

    subprocess.call(['bash', '-c', 'source ' + zephyr_env_path + '; cd build; ninja menuconfig'])
