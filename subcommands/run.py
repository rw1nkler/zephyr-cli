import os
import subprocess
# import shutil
# import pathlib

from os.path import join, getsize

def run(path, args):
    APP_DIR = args.app_dir
    ZEPHYR_BASE = os.getenv('ZEPHYR_BASE')

    # print(APP_DIR)


    # print(ZEPHYR_BASE)
    zephyr_env_path = ZEPHYR_BASE + '/zephyr-env.sh'
    subprocess.call(['bash', '-c', 'source ' + zephyr_env_path + '; mkdir build; cd build; cmake -GNinja -DBOARD=stm32l476g_disco ..; ninja flash'])
    # subprocess.call(['bash', '-c', 'source ' + zephyr_env_path + '; mkdir build; cd build; cmake -GNinja -DBOARD=esp32 ..; ninja flash'])

    # for root, dirs, files in os.walk(APP_DIR):
    #   print(root, "consumes", end=" ")
    #   print(sum(getsize(join(root, name)) for name in files), end=" ")
    #   print("bytes in", len(files), "non-directory files")

    #
    # if os.path.exists(APP_DIR + '/build'):
    #     print("Use zephyr clean first!")
    # else:
    #     os.mkdir(APP_DIR + '/build')
