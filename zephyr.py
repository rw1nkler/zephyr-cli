#!/usr/bin/python3

import os
import argparse

import subcommands.new as zphr_new
import subcommands.run as zphr_run
import subcommands.clean as zphr_clean
import subcommands.config as zphr_config

ZEPHYR_CLI_DIR = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="action")

new_parser = subparsers.add_parser('new', help = "create new application")
new_parser.add_argument("target_dir", help = "directory for new app", nargs = "?", default= os.getcwd() + '/new-app')
new_parser.set_defaults(func=zphr_new.new)

build_parser = subparsers.add_parser('run', help = "builds app")
build_parser.add_argument("app_dir", help = "apps directory", nargs = "?", default= os.getcwd())
build_parser.set_defaults(func=zphr_run.run)

clean_parser = subparsers.add_parser('clean', help = "cleans app")
clean_parser.set_defaults(func=zphr_clean.clean)

config_parser = subparsers.add_parser('config', help = "configures app")
config_parser.set_defaults(func=zphr_config.config)

args = parser.parse_args()

if args.action == None:
    parser.print_help()
    exit()
else:
    args.func(ZEPHYR_CLI_DIR, args)
